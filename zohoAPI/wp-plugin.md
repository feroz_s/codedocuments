# Zoho Plugin!

This integration, which is in the form of a plugin, allows you to easily embed your Contact pages and payment Lead or Deal in your WordPress site.


# Installation

 1. Install the zoho plugin pro
 2. Under settings tab add the {License Key}
 3. Zoho Accounts
	 1. Add New Account
	 2. Account Name (Account #1)
	 3. Click on Login with Zoho Button
	 4. Authorization with CRM administrator privilege
	 5. Test Connection

## Payment (Lead or Deal) CRM

 - Upon payment Failed, add LEAD
 - Upon payment Success, add DEAL

> File Changes:

|Mode                |Description
|-------------|---------------------------------------|
|File Name		| `functions.php`                       |
|Path         | `wp-content/themes/videotube-child/`  |

**Line Number :** 4

**Action:** Add

**CODE:**

    if(class_exists('vxcf_zoho')){
    	require_once(WP_PLUGIN_DIR.'/cf7-zoho-pro/cf7-zoho-pro.php');
    }
    
	$utm_source_cookie_name = "UTM_Source";

	if(isset($_GET['utm_source']) && !empty($_GET['utm_source'])) {
		$utm_source_cookie = $_GET['utm_source'];
		utm_gdpr($utm_source_cookie_name, $utm_source_cookie);
	}

	$utm_medium_cookie_name = "UTM_Medium";

	if(isset($_GET['utm_medium']) && !empty($_GET['utm_medium']) ) {
		$utm_medium_cookie = $_GET['utm_medium'];
		utm_gdpr($utm_medium_cookie_name, $utm_medium_cookie);
	}

	$utm_campaign_cookie_name = "UTM_Campaign";

	if(isset($_GET['utm_campaign']) && !empty($_GET['utm_campaign'])) {
		$utm_campaign_cookie = $_GET['utm_campaign'];
		utm_gdpr($utm_campaign_cookie_name, $utm_campaign_cookie);
	}

	if(isset($_GET['lead_owner']) && !empty($_GET['lead_owner'])) {
		$utm_lead_owner = 'lead_owner';
		$lead_owner = $_GET['lead_owner'];
		utm_gdpr($utm_lead_owner, $lead_owner);
	}

	if(isset($_GET['lead_type']) && !empty($_GET['lead_type'])) {
		$utm_lead_type = 'lead_type';
		$lead_type = $_GET['lead_type'];
		utm_gdpr($utm_lead_type, $lead_type);
	}

	$utm_keyword_cookie_name = "UTM_Keyword";
	if(isset($_GET['utm_keyword']) && !empty($_GET['utm_keyword'])) {
		$utm_keyword_cookie = $_GET['utm_keyword'];
		utm_gdpr($utm_keyword_cookie_name, $utm_keyword_cookie);
	}
    

**Line Number :** EOF file

**Action:** Add

**CODE:**

	/* Zoho CRM
	*	Convert Lead to Deal
	*/
	add_action('woocommerce_after_order_object_save', 'convert_lead_to_deal_in_zoho_crm', 15, 2);

	function convert_lead_to_deal_in_zoho_crm($order_id, $order){
		$order = wc_get_order($order_id);
		$orderData = $order->get_data();
		$firstName = isset($orderData['billing']) && strlen($orderData['billing']['first_name']) > 0 ? $orderData['billing']['first_name'] : "";
		$lastName = isset($orderData['billing']['last_name']) && strlen($orderData['billing']['last_name']) > 0 ? $orderData['billing']['last_name'] : "";
		$phoneNumber = isset($orderData['billing']['phone']) && strlen($orderData['billing']['phone']) > 0 ? $orderData['billing']['phone'] : "";
		$email = isset($orderData['billing']['email']) && strlen($orderData['billing']['email']) > 0 ? $orderData['billing']['email'] : "";
		$city = isset($orderData['billing']['city']) && strlen($orderData['billing']['city']) > 0 ? $orderData['billing']['city'] : "";
		$state = isset($orderData['billing']['state']) && strlen($orderData['billing']['state']) > 0 ? $orderData['billing']['state'] : "";
		$country = isset($orderData['billing']['country']) && strlen($orderData['billing']['country']) > 0 ? $orderData['billing']['country'] : "";
		$zipCode = isset($orderData['billing']['postcode']) && strlen($orderData['billing']['postcode']) > 0 ? $orderData['billing']['postcode'] : "";
		$street = isset($orderData['billing']['address_1']) && strlen($orderData['billing']['address_1']) > 0 ? $orderData['billing']['address_1'] : "";

		$isMultipleCourseOpted = false;
		$orderItems = [];
		foreach($order->get_items() as $item_id => $item){
			$course_code = get_post_meta($item->get_product_id(),'larid',true);
			if(empty($course_code)) {
				$course_code = $item->get_id();
			}
			$orderItems[$course_code] = $item->get_name();
			$total = floatval($total) + floatval($item->get_total());
		}
		if(count($orderItems) > 1) $isMultipleCourseOpted = true;
		$items = implode(',', $orderItems);
		$clientIp = get_client_ip();

		if( in_array( $order->get_status(), ['processing', 'successful'] ) ) {
			$leadID = get_utm_gdpr('tempLeadID');
			//"2098032000439311764";
			if($leadID == '') {
				file_put_contents(get_home_path()."wp-content/uploads/crm_lead_deal_error.log", PHP_EOL . PHP_EOL . "Missing LeadID on " . date("l jS \of F Y h:i:s A") . PHP_EOL . json_encode($orderData, true) . PHP_EOL . PHP_EOL . 'Status: ' . json_encode($order->get_status(), true) . PHP_EOL, FILE_APPEND);
				return FALSE;
			}
			$dealName = $firstName.$lastName;
			$cf7Zoho = new vxcf_zoho;
			$info = $cf7Zoho->get_info(2);
			$api = $cf7Zoho->get_api($info);
			$tokenInformations = $api->get_token();
			$accessToken = $tokenInformations['access_token'];

			if($isMultipleCourseOpted) {
				$courseOptedKey = 'Courses_Enrolled';
				$items = explode(',', $items);
			}else{
				$courseOptedKey = 'Course_Enroled';
			}
			$courseCodes = array_keys($orderItems);
			$payment_method_string = '';
			$payment_method = $order->get_payment_method();
			if ( $payment_method ) {
				/* translators: %s: payment method */
				$payment_method_string = sprintf(
					__( 'Payment via %s', 'woocommerce' ),
					esc_html( isset( $payment_gateways[ $payment_method ] ) ? $payment_gateways[ $payment_method ]->get_title() : $payment_method )
				);

				if ( $transaction_id = $order->get_transaction_id() ) {
					if ( isset( $payment_gateways[ $payment_method ] ) && ( $url = $payment_gateways[ $payment_method ]->get_transaction_url( $order ) ) ) {
						$payment_method_string .= ' (<a href="' . esc_url( $url ) . '" target="_blank">' . esc_html( $transaction_id ) . '</a>)';
					} else {
						$payment_method_string .= ' (' . esc_html( $transaction_id ) . ')';
					}
				}
			}

			$Source = get_utm_gdpr('UTM_Source');
			$Campaign =  get_utm_gdpr('UTM_Campaign');
			$UTMKeyword =  get_utm_gdpr('UTM_Keyword');
			$Medium = get_utm_gdpr('UTM_Medium');
			if($Source ==''){   $Source = "Organic"; }
			$items = implode(',', $orderItems);

			$requestBody = $recordArray = $recordObject = array();
			$recordObject["overwrite"]=false;
			$recordObject["notify_lead_owner"]=false;
			$recordObject["notify_new_entity_owner"]=false;
			$recordObject["Deals"] = array(
				"Deal_Name" => $dealName,
				"Closing_Date" => date("Y-m-d"),
				"Stage" => "Closed Won",
				"Pipeline" => "Standard (Standard)",
				"Amount" => (float) $order->get_total(),
				"Description" => "Order #" . $order->get_order_number() . ' ' . $payment_method_string,
				"Course_Enroled" => $items,
				"Courses_Enrolled" => $items,
				"Lead_ID" => $leadID,
				"Proforma_Invoice" => get_home_url() . "/order-details/?order=" . $order->get_order_number(),
				"ERP_Pay_Request_ID" => trim(" " . $order->get_order_number()),
				"Lead_Source_3" => "Referral",
			);
			$recordArray[] = $recordObject;
			$requestBody["data"] = $recordArray;

			$responseData = callZohoApiConvertToDeal(json_encode($requestBody, true), $accessToken, $leadID);

			file_put_contents(get_home_path()."wp-content/uploads/crm_lead_deal.log", PHP_EOL . PHP_EOL . "Convert Lead to Deal on " . date("l jS \of F Y h:i:s A") . PHP_EOL . json_encode($requestBody, true) . PHP_EOL . PHP_EOL . 'Status: ' . json_encode($responseData, true) . PHP_EOL . PHP_EOL, FILE_APPEND);

			utm_gdpr("tempLeadID", NULL);
			utm_gdpr("tempCartRunID", NULL);

		}

	}

	add_action( 'woocommerce_add_to_cart', 'after_remove_product_from_cart', 10, 6 ); 
	add_action( 'woocommerce_cart_item_removed', 'after_remove_product_from_cart', 10, 2 );
	function after_remove_product_from_cart($removed_cart_item_key, $cart) {
		utm_gdpr("tempLeadID", NULL);
		utm_gdpr("tempCartRunID", NULL);
		file_put_contents(get_home_path()."wp-content/uploads/crm_lead.log", PHP_EOL . "Reset CartRunID", FILE_APPEND);
	}

	add_action( 'woocommerce_review_order_after_order_total', 'create_lead_in_zoho_crm', 10, 2 );
	function create_lead_in_zoho_crm(){

		if(WC()->cart->is_empty()) {
			return;
		}

		$total = 0;
		$cartItemsKey = '';
		$cartInfo = WC()->cart->get_cart();

		$firstName      =   WC()->cart->get_customer()->get_billing_first_name();
		$lastName       =   WC()->cart->get_customer()->get_billing_last_name();
		$phoneNumber    =   WC()->cart->get_customer()->get_billing_phone();
		$email          =   WC()->cart->get_customer()->get_billing_email();
		$city           =   WC()->cart->get_customer()->get_billing_city();
		$state          =   WC()->cart->get_customer()->get_billing_state();
		$country        =   WC()->cart->get_customer()->get_billing_country();
		$zipCode        =   WC()->cart->get_customer()->get_billing_postcode();
		$address        =   WC()->cart->get_customer()->get_billing_address();
		$address1       =   WC()->cart->get_customer()->get_billing_address_2();
		$street         =   trim($address .' '. $address1);


		$isMultipleCourseOpted = false;
		$orderItems = [];
		foreach(WC()->cart->get_cart() as $cart_item){
			$product = $cart_item['data'];
			$course_code = get_post_meta($product->get_id(),'larid',true);
			if(empty($course_code)) {
				$course_code = $product->get_id();
			}
			$orderItems[$course_code] = $product->get_name();
			$total = floatval($total) + floatval($product->get_price());
		}

		$cartItemsKey = array_keys($orderItems);
		$cartItemsKey = implode('|', $cartItemsKey);
		$tempCartRunID = get_utm_gdpr('tempCartRunID');

		if($tempCartRunID) {
			if($tempCartRunID === $cartItemsKey) {
				file_put_contents(get_home_path()."wp-content/uploads/crm_lead.log", PHP_EOL . "Lead already created " . $tempCartRunID, FILE_APPEND);
				return;
			}
		}


		if(count($orderItems) > 1) $isMultipleCourseOpted = true;
		$items = implode(',', $orderItems);
		$clientIp = get_client_ip();

		$cf7Zoho = new vxcf_zoho;
		$info = $cf7Zoho->get_info(2);
		$api = $cf7Zoho->get_api($info);
		$tokenInformations = $api->get_token();
		$accessToken = $tokenInformations['access_token'];
		
		if($isMultipleCourseOpted) {
			$courseOptedKey = 'Courses_Opted';
			$items = explode(',', $items);
		}else{
			$courseOptedKey = 'Course_Opted';
		}
		$courseCodes = array_keys($orderItems);
		$Source = get_utm_gdpr('UTM_Source');
		$Campaign =  get_utm_gdpr('UTM_Campaign');
		$UTMKeyword =  get_utm_gdpr('UTM_Keyword');
		$Medium = get_utm_gdpr('UTM_Medium');
		if($Source ==''){   $Source = "Organic"; }

		$jsonPostField = [
		'data' => [
			[
				'First_Name' => '',
				'Last_Name' => trim($firstName.' '.$lastName),
				'Mobile' => $phoneNumber,
				'Phone' => $phoneNumber,
				$courseOptedKey => $items,
				'Lead_Source' => "Online Store",
				'Email' =>  $email,
				'Street'=> $street,
				'City' =>  $city,
				'State' => $state,
				'Country' => $country,
				'Zip_Code' => $zipCode,
				'Customer_IP' => $clientIp,
				'Location_Link_GMaps' => get_home_url() . "/ip_address.php?IP=$clientIp",
				'Created_Via' => get_home_url(),
				'Description' => "Order Created in website and payment is pending or failed so system creating as LEAD",
				'HDesc' => NULL,
				'Lead_Source_3' => 'Referral',
				'Lead_Type' => "ESales",
				'Owner' => '2098032000371933001',
				'Expected_Closure_Amount' => $total,
				'Source'    =>  $Source,
				'Campaign'  =>  $Campaign,
				'UTMKeyword'=>  $UTMKeyword,
				'Medium'    =>  $Medium,
			]
		],
		'duplicate_check_fields' => [
			'Email',
			'Mobile'
		],
		'trigger' => [
				'workflow'
			]
		];

		if($isMultipleCourseOpted) {
			$jsonPostField['data'][0]['Course_Opted'] = "yes";
		}

		$responseData = callZohoApi(json_encode($jsonPostField,true), $accessToken, 'Leads');
		$_responseData = json_decode($responseData);
		$leadID = $_responseData->data[0]->details->id;

		utm_gdpr("tempLeadID", $leadID);

		file_put_contents(get_home_path()."wp-content/uploads/crm_lead.log",
			PHP_EOL . "Lead Created on " . date("l jS \of F Y h:i:s A") . PHP_EOL . json_encode($jsonPostField, true) . PHP_EOL .
			PHP_EOL . print_r($responseData, true) . PHP_EOL . "Lead ID:" . $leadID . PHP_EOL . PHP_EOL . 'Token: ' . PHP_EOL .
			json_encode($accessToken, true) . PHP_EOL,
			FILE_APPEND
		);

		if($leadID) {
			utm_gdpr("tempCartRunID", trim($cartItemsKey));

			$cartItemsKey = explode("|", $cartItemsKey);
			$crmProductIDs = [];
			foreach($cartItemsKey as $v) {
				$crmProductIDs[] = array("id" => $v);
			}
			$productJsonData = [
				'data' => $crmProductIDs
			];
			$response = putProductToLeadAPI(json_encode($productJsonData,true), $accessToken, $leadID);
			file_put_contents(get_home_path()."wp-content/uploads/crm_lead_product.log", PHP_EOL . PHP_EOL . "Products added to lead on " . date("l jS \of F Y h:i:s A") . PHP_EOL .  PHP_EOL .  print_r($response, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
			
		}

		remove_action( 'woocommerce_checkout_before_order_review', 'record_log', 10, 2 );

	}

	function zoho_crm_lead_generate($user_array){
		$cf7Zoho = new vxcf_zoho;
		$info = $cf7Zoho->get_info(2);
		$api = $cf7Zoho->get_api($info);
		$tokenInformations = $api->get_token();
		$accessToken = $tokenInformations['access_token'];
		$clientIp = get_client_ip();

		$city = $country = $state = $zipCode = "";
		if($user_array['City']) {
			$city = $user_array['City'];
			$state = $user_array['State'];
			$country = $user_array['Country'];
			$zipCode = $user_array['Zip_Code'];
		} else {
			$jsonPostField = fn_get_geo_location($clientIp);
			$userLocation = json_decode($jsonPostField,true);
			if (!empty($userLocation) && is_array($userLocation) && count($userLocation) > 0) {
				$city = isset($userLocation['city']) && isset($userLocation['city']['names']) ? $userLocation['city']['names']['en'] : "";
				$state = isset($userLocation['subdivisions']) ? $userLocation['subdivisions'][0]['names']['en'] : "";
				$country = isset($userLocation['country']) ? $userLocation['country']['names']['en'] : "";
				$zipCode = isset($userLocation['postal']) ? $userLocation['postal']['code'] : "";
			}
		}

		$firstLastName = $user_array['Last_Name'];
		if(isset($user_array['Created_Via'])) {
			if(empty($user_array['Created_Via']) || $user_array['Created_Via']== ''){
				$user_array['Created_Via'] = get_home_url();
			}
		} else {
			$user_array['Created_Via'] = get_home_url();
		}

		$jsonPostField = [
			'data' => [
				[
					'First_Name' => '',
					'Last_Name' => $firstLastName,
					'Mobile' => $user_array['mobile_phone_num'],
					'Phone' => $user_array['mobile_phone_num'],
					'Lead_Source' => "Online Store",
					'Qualification'=> $user_array['Qualification'],
					'Course_Opted' => $user_array['Course_Opted'],
					'Email' =>  $user_array['Email'],
					'City' =>  $city,
					'State' => $state,
					'Country' => $country,
					'Zip_Code' =>$zipCode,
					'Customer_IP' => $clientIp,
					'Location_Link_GMaps' => get_home_url() . "/ip_address.php?IP=$clientIp",
					'Created_Via' => $user_array['Created_Via'],
					'Description' => "Enquire form Web to Lead",
					'Lead_Type' => $user_array['Lead_Type'],
					'Expected_Closure_Amount' => $user_array['Expected_Closure_Amount'],
					'Lead_Stage' => $user_array['Lead_Stage'],
					'Lead_Source_2' => $user_array['Lead_Source_2'],
					'Potential_Close_Time' => $user_array['Potential_Close_Time'],
					'Campaign' => $user_array['Campaign'],
					'Medium' => $user_array['Medium'],
					'UTMKeyword' => $user_array['UTMKeyword'],
					'Owner' => '2098032000371933001',
					'Source' => 'Organic',
					'Lead_Source_3' => 'Toll Free',
					'HDesc' => 'Dummy HDesc',
				]
			],
			'duplicate_check_fields' => [
				'Email',
				'Mobile'
			],
			'trigger' => [
				'workflow'
			]
		];

		file_put_contents(get_home_path()."wp-content/uploads/crm_lead_generate.log", PHP_EOL . PHP_EOL . "Func [zoho_crm_lead_generate] on " . date("l jS \of F Y h:i:s A") . PHP_EOL . json_encode($jsonPostField, true) . PHP_EOL . PHP_EOL .
		PHP_EOL . json_encode($user_array, true) . PHP_EOL . PHP_EOL . 'Token: ' . $accessToken . PHP_EOL . PHP_EOL, FILE_APPEND);

		$response = callZohoApi(json_encode($jsonPostField,true), $accessToken, 'Leads');

		$_responseData = json_decode($response);
		$leadID = $_responseData->data[0]->details->id;

		if($leadID) {
			//Search Products by code
			$productResp = getProductIDByCodeAPI($accessToken, $user_array['Course_Code']);
			if($productResp) {
				$_respProduct = json_decode($productResp);
				$productID = $_respProduct->data[0]->id;
				$productJsonData = [
					'data' => [
						[
							'id' => $productID,
						]
					]
				];
				$response = putProductToLeadAPI(json_encode($productJsonData,true), $accessToken, $leadID);
				file_put_contents(get_home_path()."wp-content/uploads/crm_lead_product.log", PHP_EOL . PHP_EOL . "Products added to lead on " . date("l jS \of F Y h:i:s A") . PHP_EOL .  PHP_EOL .  json_encode($response, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
			} else {
				file_put_contents(get_home_path()."wp-content/uploads/crm_lead_error.log", PHP_EOL . PHP_EOL . "Process [Products] on " . date("l jS \of F Y h:i:s A") . PHP_EOL . PHP_EOL . PHP_EOL . "Product ID: NOT FOUND" . PHP_EOL .  json_encode($productResp, true) . PHP_EOL . PHP_EOL, FILE_APPEND);
			}
		}

	}

	function getProductIDByCodeAPI($accessToken, $code) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://www.zohoapis.com/crm/v2/Products/search?criteria='.urlencode($code),
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_SSL_VERIFYHOST => false, //TODO : remove on live
		CURLOPT_SSL_VERIFYPEER => false, //TODO : remove on live
		CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$accessToken
			),
		));
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
			$error = curl_error($curl);
			file_put_contents(get_home_path()."wp-content/uploads/crm_lead_error.log", "Getting Products " . print_r($error, true) . PHP_EOL, FILE_APPEND);
		}
		curl_close($curl);
		return $response;
	}

	function putProductToLeadAPI($json, $accessToken, $leadID) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://www.zohoapis.com/crm/v2/Leads/'.$leadID.'/Products',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_SSL_VERIFYHOST => false, //TODO : remove on live
		CURLOPT_SSL_VERIFYPEER => false, //TODO : remove on live
		CURLOPT_CUSTOMREQUEST => 'PUT',
		CURLOPT_POSTFIELDS => $json,
			CURLOPT_HTTPHEADER => array(
					'Authorization: Bearer '.$accessToken,
					'Content-Type: application/json'
			),
		));
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
			$error = curl_error($curl);
			file_put_contents(get_home_path()."wp-content/uploads/crm_lead_error.log", "Adding product to Lead Error " . print_r($error, true) . PHP_EOL, FILE_APPEND);
		}
		curl_close($curl);
		return $response;
	}

	function callZohoApi($json, $accessToken, $module) {
		/*if($module == "Leads") { $module .= "/upsert"; }*/
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => 'https://www.zohoapis.com/crm/v2/'.$module.'/upsert',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_SSL_VERIFYHOST => false, //TODO : remove on live
		CURLOPT_SSL_VERIFYPEER => false, //TODO : remove on live
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => $json,
			CURLOPT_HTTPHEADER => array(
					'Authorization: Bearer '.$accessToken,
					'Content-Type: application/json'
			),
		));
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
			$error = curl_error($curl);
			file_put_contents(get_home_path()."wp-content/uploads/crm_lead_error.log", "Lead upsert Error " . print_r($error, true) . PHP_EOL, FILE_APPEND);
		}
		curl_close($curl);
		return $response;
	}

	function callZohoApiConvertToDeal($json, $accessToken, $leadID) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://www.zohoapis.com/crm/v2/Leads/".trim($leadID)."/actions/convert",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_SSL_VERIFYHOST => false, //TODO : remove on live
		CURLOPT_SSL_VERIFYPEER => false, //TODO : remove on live
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => $json,
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$accessToken,
				'Content-Type: application/json'
			),
		));
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
			$error = curl_error($curl);
			file_put_contents(get_home_path()."wp-content/uploads/crm_lead_error.log", "Lead Error " . print_r($error, true) . PHP_EOL, FILE_APPEND);
		}
		curl_close($curl);
		return $response;
	}

	function fn_get_geo_location($clientIP) {
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => 'https://geoip.maxmind.com/geoip/v2.1/city/'.$clientIP.'?pretty',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_SSL_VERIFYHOST => false, //TODO : remove on live
			CURLOPT_SSL_VERIFYPEER => false, //TODO : remove on live
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Basic MTM1NjI2OlFiVlE2akt3VW9zSG0xdks='
			),
		));
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
			$error = curl_error($curl);
			file_put_contents(get_home_path()."wp-content/uploads/geo-location.log", "GEO LOCATION ERROR " . print_r($error, true) . PHP_EOL, FILE_APPEND);
		}
		curl_close($curl);
		return $response;
	}
	/* Zoho CRM - END */

	function get_client_ip() {
		$ipaddress = '';
		if(isset($_SERVER['HTTP_CF_CONNECTING_IP']))
		$ipaddress = $_SERVER['HTTP_CF_CONNECTING_IP'];
		else if (isset($_SERVER['HTTP_CLIENT_IP']))
				$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
				$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
				$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
				$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
				$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}

	// hide update, notifications, menus
	function remove_admin_menu_items() {
		remove_submenu_page( 'index.php', 'update-core.php');
	}
	function remove_core_updates(){
		global $wp_version;
		return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
	}
	function remove_dashboard_widgets() {
		remove_meta_box('dashboard_primary', 'dashboard', 'side' );
		remove_meta_box('dashboard_secondary', 'dashboard', 'side' );
		remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'side' );
		remove_meta_box('dashboard_activity', 'dashboard', 'normal');
	}
	add_filter('pre_site_transient_update_core','remove_core_updates'); //hide updates for WordPress itself
	add_filter('pre_site_transient_update_plugins','remove_core_updates'); //hide updates for all plugins
	add_filter('pre_site_transient_update_themes','remove_core_updates'); //hide updates for all themes
	add_action('wp_dashboard_setup', 'remove_dashboard_widgets');
	add_action('admin_menu', 'remove_admin_menu_items', 999);

___

> File Changes:

|Mode                |Description
|---------------|-------------------------------|
|File Name		|`cf7-zoho-pro.php`            |
|Path          	|`wp-content/plugins/cf7-zoho-pro/`          |

**Line Number :** 62/63 declare variable area

**Action:** Add

**CODE:**

   	public static $geo_location_details;

**Line :** setup_main function

**Action:** Add

**CODE:**

#### function setup_main()

    $curl = curl_init();
    $clientIp = isset($_SERVER["HTTP_CF_CONNECTING_IP"]) && $_SERVER["HTTP_CF_CONNECTING_IP"] != "" ? $_SERVER["HTTP_CF_CONNECTING_IP"] : $_SERVER['REMOTE_ADDR'];
    $token = base64_encode(GEO_LOCATION_API_CLIENT_ID.':'.GEO_LOCATION_API_PASSWORD);
    curl_setopt_array($curl, array(
      CURLOPT_URL => GEO_LOCATION_API_URL.$clientIp.'?pretty',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => array(
      'Authorization: Basic '.$token
      ),
    ));

    $response = curl_exec($curl);
    curl_close($curl);
    self::$geo_location_details = json_decode($response,true);

**Line :** form_submitted function -> After "if(is_array($tags)){" condition

**Action:** Add

**CODE:**

#### form_submitted() function

    $userLocation = self::$geo_location_details;
    $city = $country = $state = $zipCode = "";
    if (!empty($userLocation) && is_array($userLocation) && count($userLocation) > 0) {
      $city = isset($userLocation['city']) && isset($userLocation['city']['names']) ? $userLocation['city']['names']['en'] : "";
      $state = isset($userLocation['subdivisions']) ? $userLocation['subdivisions'][0]['names']['en'] : "";
      $country = isset($userLocation['country']) ? $userLocation['country']['names']['en'] : "";
      $zipCode = isset($userLocation['postal']) ? $userLocation['postal']['code'] : "";
    }
    $locationData = ['City' => $city, 'State' => $state, 'Country' => $country, 'Zip_Code' => $zipCode];

**Line :** form_submitted function After -> $val=$submission->get_posted_data($name);

**Action:** Add

**CODE:**

#### form_submitted() function

    if(in_array($name, ["City", "State", "Country", "Zip_Code"])) {
	    $val = $locationData[$name];
    }